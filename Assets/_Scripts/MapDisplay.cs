﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapDisplay : MonoBehaviour
{
    public Renderer _textureRender;
    public Terrain _terrainRender;

    public void DrawTexture(Texture2D texture)
    {
        _textureRender.sharedMaterial.mainTexture = texture;
        _textureRender.transform.localScale = new Vector3(10, 1, 10);
    }

    public void DrawNoiseOnTerrain(float[,] heightMap, TerrainType[] regions)
    {
        _terrainRender.terrainData.heightmapResolution = heightMap.GetLength(0);
        _terrainRender.terrainData.size = new Vector3(100, 25, 100);
        _terrainRender.terrainData.SetHeights(0, 0, heightMap);

    }

    public void DrawTextureOnTerrain(TerrainType[] regions)
    {
        SplatPrototype[] terrainTextures = new SplatPrototype[regions.Length];
        for (int i = 0; i < terrainTextures.Length; i++)
        {
            terrainTextures[i] = new SplatPrototype
            {
                texture = regions[i].regionTexture,
                tileSize = new Vector2(15, 15)
            };
        }
        //_terrainRender.terrainData.splatPrototypes = terrainTextures;
        float[,,] splatmapData = new float[_terrainRender.terrainData.alphamapWidth, _terrainRender.terrainData.alphamapHeight, _terrainRender.terrainData.alphamapLayers];
        for (int y = 0; y < _terrainRender.terrainData.alphamapHeight; y++)
        {
            for (int x = 0; x < _terrainRender.terrainData.alphamapWidth; x++)
            {
                // Normalise x/y coordinates to range 0-1 
                float y_01 = (float)y / (float)_terrainRender.terrainData.alphamapHeight;
                float x_01 = (float)x / (float)_terrainRender.terrainData.alphamapWidth;

                // Sample the height at this location (note GetHeight expects int coordinates corresponding to locations in the heightmap array)
                float height = _terrainRender.terrainData.GetHeight(Mathf.RoundToInt(y_01 * _terrainRender.terrainData.heightmapHeight), Mathf.RoundToInt(x_01 * _terrainRender.terrainData.heightmapWidth));

                bool colorSet = false;
                for (int i = 0; i < regions.Length; i++)
                {
                    
                    if (height <= (_terrainRender.terrainData.size.y * regions[i].height) && !colorSet)
                    {
                        splatmapData[x,y,i] = 1;
                        colorSet = true;
                    }
                    else
                    {
                        splatmapData[x, y, i] = 0;
                    }
                }

            }
        }

        // Finally assign the new splatmap to the terrainData:
        _terrainRender.terrainData.SetAlphamaps(0, 0, splatmapData);

        //for (int y = 0; y < _mapHeight; y++)
        //{
        //    for (int x = 0; x < _mapWidth; x++)
        //    {
        //        float currentHeight = noiseMap[x, y];
        //        for (int i = 0; i < _regions.Length; i++)
        //        {
        //            if (currentHeight <= _regions[i].height)
        //            {
        //                colorMap[y * _mapWidth + x] = _regions[i].color;
        //                break;
        //            }
        //        }
        //    }
        //}
    }
}
