﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public enum DrawMode { NoiseMap, ColorMap};
    public DrawMode _drawMode;

    public int _mapWidth;
    public int _mapHeight;
    public float _noiseScale;

    public int _octaves;
    [Range(0, 1)]
    public float _persistance;
    public float _lacunarity;

    public int _seed;
    public Vector2 _offset;

    public bool _autoUpdate;

    public TerrainType[] _regions;

    public void GenerateMap()
    {
        float[,] noiseMap = Noise.GenerateNoiseMap(_mapWidth, _mapHeight, _seed, _noiseScale, _octaves, _persistance, _lacunarity, _offset);

        Color[] colorMap = new Color[_mapWidth * _mapHeight];
        for(int y = 0; y < _mapHeight; y++)
        {
            for(int x = 0; x < _mapWidth; x++)
            {
                float currentHeight = noiseMap[x, y];
                for(int i = 0; i < _regions.Length; i++)
                {
                    if(currentHeight <= _regions[i].height)
                    {
                        colorMap[y * _mapWidth + x] = _regions[i].color;
                        break;
                    }
                }
            }
        }

        MapDisplay display = GetComponent<MapDisplay>();
        if(_drawMode == DrawMode.NoiseMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromHeightMap(noiseMap));
        }
        else if(_drawMode == DrawMode.ColorMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromColorMap(colorMap, _mapWidth, _mapHeight));
        }

        display.DrawNoiseOnTerrain(noiseMap, _regions);
    }

    public void GenerateSplatmap()
    {
        MapDisplay display = GetComponent<MapDisplay>();
        display.DrawTextureOnTerrain(_regions);
    }

    private void OnValidate()
    {
        if(_mapWidth < 1)
        {
            _mapWidth = 1;
        }
        if(_mapHeight < 1)
        {
            _mapHeight = 1;
        }
        if(_lacunarity < 1)
        {
            _lacunarity = 1;
        }
        if(_octaves < 0)
        {
            _octaves = 0;
        }
    }
}

[System.Serializable]
public struct TerrainType
{
    public string name;
    public float height;
    public Color color;
    public Texture2D regionTexture;
}
